import { NgModule } from '@angular/core';
import { ParallaxHeaderDirective } from './parallax-header/parallax-header';
@NgModule({
	declarations: [ParallaxHeaderDirective,
    ParallaxHeaderDirective],
	imports: [],
	exports: [ParallaxHeaderDirective,
    ParallaxHeaderDirective]
})
export class DirectivesModule {}
