export interface IHeatShield {
    material: string;
    size_meters: number;
    temp_degrees: number;
    dev_partner: string;
}

export interface IThrust {
    kN: number;
    lbf: number;
}

export interface IThruster {
    type: string;
    amount: number;
    pods: number;
    fuel_1: string;
    fuel_2: string;
    thrust: IThrust;
}

export interface ILaunchPayloadMass {
    kg: number;
    lb: number;
}

export interface ILaunchPayloadVol {
    cubic_meters: number;
    cubic_feet: number;
}

export interface IReturnPayloadMass {
    kg: number;
    lb: number;
}

export interface IReturnPayloadVol {
    cubic_meters: number;
    cubic_feet: number;
}

export interface IPayloadVolume {
    cubic_meters: number;
    cubic_feet: number;
}

export interface IPressurizedCapsule {
    payload_volume: IPayloadVolume;
}

export interface ITrunkVolume {
    cubic_meters: number;
    cubic_feet: number;
}

export interface ICargo {
    solar_array: number;
    unpressurized_cargo: boolean;
}

export interface ITrunk {
    trunk_volume: ITrunkVolume;
    cargo: ICargo;
}

export interface IHeightWTrunk {
    meters: number;
    feet: number;
}

export interface IDiameter {
    meters: number;
    feet: number;
}

export interface ICapsule {
    id: string;
    name: string;
    type: string;
    active: boolean;
    crew_capacity: number;
    sidewall_angle_deg: number;
    orbit_duration_yr: number;
    heat_shield: IHeatShield;
    thrusters: IThruster[];
    launch_payload_mass: ILaunchPayloadMass;
    launch_payload_vol: ILaunchPayloadVol;
    return_payload_mass: IReturnPayloadMass;
    return_payload_vol: IReturnPayloadVol;
    pressurized_capsule: IPressurizedCapsule;
    trunk: ITrunk;
    height_w_trunk: IHeightWTrunk;
    diameter: IDiameter;
}
