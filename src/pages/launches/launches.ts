import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController} from 'ionic-angular';
import { ILaunch } from '../../models/ILaunch';
import { SpaceXProvider } from '../../providers/space-x/space-x';
import { LaunchDetailsPage } from "../launch-details/launch-details";
import { FiltersModalPage } from "./filters-modal/filters-modal";

@IonicPage()
@Component({
  selector: 'page-launches',
  templateUrl: 'launches.html',
})
export class LaunchesPage {

  pastLaunches : ILaunch[] = [];
  upcomingLaunches : ILaunch[] = [];
  nextLaunch : ILaunch;
  period : string = "next";
  timer;
  filters : object = {
    order : "desc",
    launch_year : "",
    launch_success : "",
    site_id : "",
    rocket_id : ""
  };
  countdownDays : number = 0;
  countdownHours : number = 0;
  countdownMinutes : number = 0;
  countdownSeconds : number = 0;

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider, private modalCtrl : ModalController) {}

  ionViewDidLoad() {
    this.spaceXProvider.getPastLaunches(this.filters).subscribe(data => this.pastLaunches = data);
    this.spaceXProvider.getUpcomingLaunches().subscribe(data => this.upcomingLaunches = data);
    this.spaceXProvider.getNextLaunch().subscribe(data => {
      this.nextLaunch = data;
      this.startCountDown();
    });
  }

  doRefreshPastLaunches(refresher) {
    this.spaceXProvider.getPastLaunches().subscribe(data => {
      this.pastLaunches = data;
      refresher.complete();
    });
  }

  doRefreshUpcomingLaunches(refresher) {
    this.spaceXProvider.getUpcomingLaunches().subscribe(data => {
      this.upcomingLaunches = data;
      refresher.complete();
    });
  }

  doRefreshNextLaunch(refresher) {
    this.spaceXProvider.getNextLaunch().subscribe(data => {
      this.nextLaunch = data;
      this.startCountDown();
      refresher.complete();
    });
  }

  startCountDown() {
    this.updateCountdown();

    this.timer = setInterval(() => {
      let countdown = this.updateCountdown();
      if (countdown < 0) {
        clearInterval(this.timer);
      }
    }, 1000);
  }

  updateCountdown() : number {
    if (!this.nextLaunch) {
      return;
    }

    let countdown = this.nextLaunch.launch_date_unix * 1000 - Date.now();
    this.countdownDays = Math.floor(countdown / (1000 * 60 * 60 * 24));
    this.countdownHours = Math.floor((countdown % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.countdownMinutes = Math.floor((countdown % (1000 * 60 * 60)) / (1000 * 60));
    this.countdownSeconds = Math.floor((countdown % (1000 * 60)) / 1000);

    return countdown;
  }

  showFilters() {
    let filtersModal = this.modalCtrl.create(FiltersModalPage, this.filters, { cssClass: 'inset-modal', showBackdrop: true, enableBackdropDismiss: true });
    filtersModal.onDidDismiss(filters => {
      this.filters = filters;
      this.spaceXProvider.getPastLaunches(this.filters).subscribe(data => {
        this.pastLaunches = data;
        this.period = "past";
      });
    });

    filtersModal.present().catch(err => console.error(err));
  }

  segmentChanged(segmentButton) {
    this.period = segmentButton.value;
  }

  viewDetails(launch: ILaunch) {
    this.navCtrl.push(LaunchDetailsPage, launch).catch(err => console.error(err));
  }
}
