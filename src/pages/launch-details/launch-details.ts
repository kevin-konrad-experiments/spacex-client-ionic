import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ILaunch, ILaunchSite, IRocket} from "../../models/ILaunch";
import {RocketDetailsPage} from "../rocket-details/rocket-details";
import {LaunchpadDetailsPage} from "../launchpad-details/launchpad-details";

@IonicPage()
@Component({
  selector: 'page-launch-details',
  templateUrl: 'launch-details.html',
})
export class LaunchDetailsPage {

  launch: ILaunch;

  constructor(private navParams: NavParams, private navCtrl: NavController) {
    this.launch = this.navParams.data;
  }

  viewRocket(rocket : IRocket) {
    this.navCtrl.push(RocketDetailsPage, rocket.rocket_id).catch(err => console.error(err));
  }

  viewLaunchSite(launchSite : ILaunchSite) {
    this.navCtrl.push(LaunchpadDetailsPage, launchSite.site_id).catch(err => console.error(err));
  }

  openURL(url: string) {
    window.open(url, '_blank');
  }
}
