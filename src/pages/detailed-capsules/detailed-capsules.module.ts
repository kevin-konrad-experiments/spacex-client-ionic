import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailedCapsulesPage } from './detailed-capsules';

@NgModule({
  declarations: [
    DetailedCapsulesPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailedCapsulesPage),
  ],
})
export class DetailedCapsulesPageModule {}
