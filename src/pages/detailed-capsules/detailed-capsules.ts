import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { IDetailsCapsule } from '../../models/IDetailsCapsule';
import { SpaceXProvider } from '../../providers/space-x/space-x';
import { DetailedCapsuleDetailsPage } from '../detailed-capsule-details/detailed-capsule-details';

@IonicPage()
@Component({
  selector: 'page-detailed-capsules',
  templateUrl: 'detailed-capsules.html',
})
export class DetailedCapsulesPage {

  detailsCapsules : IDetailsCapsule [] = [];

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getDetailsCapsule().subscribe(data => this.detailsCapsules = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getDetailsCapsule().subscribe(data => {
      this.detailsCapsules = data;
      refresher.complete();
    });
  }

  viewDetails(detailsCapsules : IDetailsCapsule){
    this.navCtrl.push(DetailedCapsuleDetailsPage,detailsCapsules).catch(err => console.error(err));
  }
}
