import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchpadDetailsPage } from './launchpad-details';
import { DirectivesModule } from "../../directives/directives.module";

@NgModule({
  declarations: [
    LaunchpadDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchpadDetailsPage),
    DirectivesModule
  ],
})
export class LaunchpadDetailsPageModule {}
