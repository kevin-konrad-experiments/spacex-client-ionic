import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailedCoresPage } from './detailed-cores';

@NgModule({
  declarations: [
    DetailedCoresPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailedCoresPage),
  ],
})
export class DetailedCoresPageModule {}
