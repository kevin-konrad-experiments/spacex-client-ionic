import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { IDetailsCapsule } from '../../models/IDetailsCapsule';

@IonicPage()
@Component({
  selector: 'page-detailed-capsule-details',
  templateUrl: 'detailed-capsule-details.html',
})
export class DetailedCapsuleDetailsPage {

  detailsCapsule : IDetailsCapsule;

  constructor(private navParams: NavParams) {
    this.detailsCapsule = this.navParams.data;
  }
}
