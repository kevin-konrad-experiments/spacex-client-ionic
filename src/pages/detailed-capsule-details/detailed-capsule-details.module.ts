import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailedCapsuleDetailsPage } from './detailed-capsule-details';

@NgModule({
  declarations: [
    DetailedCapsuleDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailedCapsuleDetailsPage),
  ],
})
export class DetailedCapsuleDetailsPageModule {}
