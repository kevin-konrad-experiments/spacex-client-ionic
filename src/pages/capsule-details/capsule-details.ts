import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import {ICapsule} from "../../models/ICapsule";

@IonicPage()
@Component({
  selector: 'page-capsule-details',
  templateUrl: 'capsule-details.html',
})
export class CapsuleDetailsPage {

  capsule: ICapsule = null;

  constructor(private navParams: NavParams) {
    this.capsule = this.navParams.data;
  }
}
