import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { SpaceXProvider } from "../../providers/space-x/space-x";
import {ILaunch} from "../../models/ILaunch";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  successChart : any = {
    type : 'doughnut',
    labels : ['Successes', 'Failures'],
    data : [0, 0],
    options: {
      legend: {position: 'bottom'}
    }
  };

  yearsChart : any = {
    type : 'bar',
    labels : ['2012', '2013', '2014', '2015', '2016', '2017', '2018'],
    data : [
      {data: [0, 0, 0, 0, 0, 0, 0], label: 'Launches'},
    ],
    options: {
      legend: {position: 'bottom'}
    }
  };

  rocketsChart : any = {
    type : 'pie',
    labels : ['Falcon 1', 'Falcon 9', 'Falcon Heavy', 'Big Falcon Rocket'],
    data : [0, 0, 0, 0],
    options: {
      legend: {position: 'bottom'}
    }
  };

  constructor(private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getPastLaunches().toPromise()
      .then(this.updateSuccessChart)
      .then(this.updateYearsChart)
      .then(this.updateRocketsChart);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getPastLaunches().toPromise()
      .then(this.updateSuccessChart)
      .then(this.updateYearsChart)
      .then(this.updateRocketsChart)
      .then(refresher.complete());
  }

  updateSuccessChart = (launches : ILaunch[]) => {
    let successData = [0, 0];

    launches.forEach(launch => {
      if (launch.launch_success) {
        successData[0]++;
      } else {
        successData[1]++;
      }
    });

    this.successChart.data = successData;
    return launches;
  };

  updateYearsChart = (launches : ILaunch[]) => {
    let yearsData = [
      { data: [0, 0, 0, 0, 0, 0, 0], label: 'Launches' }
    ];

    launches.forEach(launch => {
      switch (launch.launch_year) {
        case "2012":
          yearsData[0].data[0]++;
          break;
        case "2013":
          yearsData[0].data[1]++;
          break;
        case "2014":
          yearsData[0].data[2]++;
          break;
        case "2015":
          yearsData[0].data[3]++;
          break;
        case "2016":
          yearsData[0].data[4]++;
          break;
        case "2017":
          yearsData[0].data[5]++;
          break;
        case "2018":
          yearsData[0].data[6]++;
          break;
      }
    });

    this.yearsChart.data = yearsData;
    return launches;
  };

  updateRocketsChart = (launches : ILaunch[]) => {
    let rocketsData = [0, 0, 0, 0];

    launches.forEach(launch => {
      switch (launch.rocket.rocket_name) {
        case "Falcon 1":
          rocketsData[0]++;
          break;
        case "Falcon 9":
          rocketsData[1]++;
          break;
        case "Falcon Heavy":
          rocketsData[2]++;
          break;
        case "Big Falcon Rocket":
          rocketsData[3]++;
          break;
      }
    });

    this.rocketsChart.data = rocketsData;
    return launches;
  };
}
