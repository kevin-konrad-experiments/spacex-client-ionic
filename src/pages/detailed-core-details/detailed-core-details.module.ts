import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailedCoreDetailsPage } from './detailed-core-details';

@NgModule({
  declarations: [
    DetailedCoreDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailedCoreDetailsPage),
  ],
})
export class DetailedCoreDetailsPageModule {}
