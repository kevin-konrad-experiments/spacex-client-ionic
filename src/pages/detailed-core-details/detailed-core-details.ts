import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { IDetailsCore } from '../../models/IDetailsCore';

@IonicPage()
@Component({
  selector: 'page-detailed-core-details',
  templateUrl: 'detailed-core-details.html',
})
export class DetailedCoreDetailsPage {

  detailsCore : IDetailsCore;

  constructor(private navParams: NavParams) {
    this.detailsCore = this.navParams.data;
  }
}
