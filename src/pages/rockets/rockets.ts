import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {SpaceXProvider} from "../../providers/space-x/space-x";
import {IRocket} from "../../models/IRocket";
import {RocketDetailsPage} from "../rocket-details/rocket-details";

@IonicPage()
@Component({
  selector: 'page-rockets',
  templateUrl: 'rockets.html',
})
export class RocketsPage {

  rockets: IRocket[] = [];

  constructor(private navCtrl: NavController, private spaceXProvider: SpaceXProvider) {}

  ionViewDidLoad() {
    this.spaceXProvider.getRockets().subscribe(data => this.rockets = data);
  }

  doRefresh(refresher) {
    this.spaceXProvider.getRockets().subscribe(data => {
      this.rockets = data;
      refresher.complete();
    });
  }

  viewDetails(rocket: IRocket) {
    this.navCtrl.push(RocketDetailsPage, rocket.id).catch(err => console.error(err));
  }
}
