import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform } from 'ionic-angular';
import { HomePage } from "../pages/home/home";
import {AboutPage} from "../pages/about/about";
import {RocketsPage} from "../pages/rockets/rockets";
import {CapsulesPage} from "../pages/capsules/capsules";
import {LaunchpadsPage} from "../pages/launchpads/launchpads";
import {LaunchesPage} from "../pages/launches/launches";
import {HistoryPage} from "../pages/history/history";
import {DetailedCoresPage} from "../pages/detailed-cores/detailed-cores";
import {DetailedCapsulesPage} from "../pages/detailed-capsules/detailed-capsules";
import {SpaceXProvider} from "../providers/space-x/space-x";
import {ILocalNotification, ILocalNotificationAction, LocalNotifications} from "@ionic-native/local-notifications";
import {LaunchDetailsPage} from "../pages/launch-details/launch-details";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  @ViewChild('content') navCtrl: NavController;
  rootPage: any = LaunchesPage;
  pages: Array<{title: string, component: any}>;
  showSplash = true;

  constructor(private platform: Platform, private spaceXApi: SpaceXProvider, private localNotifications : LocalNotifications) {
    this.initializeApp();

    this.pages = [
      { title: 'Launches', component: LaunchesPage },
      { title: 'Dashboard', component: HomePage },
      { title: 'History', component: HistoryPage },
      { title: 'Capsules', component: CapsulesPage },
      { title: 'Launchpads', component: LaunchpadsPage },
      { title: 'Rockets', component: RocketsPage },
      { title: 'Detailed capsules', component: DetailedCapsulesPage },
      { title: 'Detailed cores', component: DetailedCoresPage },
      { title: 'About', component: AboutPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.hideSplashScreen();
      // this.statusBar.styleDefault();
      this.initNotification();
    });
  }

  hideSplashScreen() {
    setTimeout(() => this.showSplash = false, 3000);
  }

  initNotification() {
    if (this.platform.is("cordova")) {
      this.displayNextLaunchNotification();
    }
  }

  openPage(page) {
    this.nav.setRoot(page.component).catch(err => console.error(err));
  }

  private convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    let date = new Date(inputFormat);
    return [ pad(date.getDate()), pad(date.getMonth()+1), date.getFullYear() ].join('/');
  }

  displayNextLaunchNotification() {
    this.spaceXApi.getNextLaunch().subscribe(nextLaunch => {
      if (!nextLaunch) {
        return;
      }

      let actions: ILocalNotificationAction[] = [
        { id: 'next-details', title: 'Details', launch: true },
        { id: 'next-ignore',  title: 'Ignore', launch: false }
      ];

      if (nextLaunch.links.reddit_campaign) {
        actions.push({ id: 'next-reddit',  title: 'Reddit', launch: false })
      }

      let options: ILocalNotification = {
        title: 'Next launch',
        text: nextLaunch.rocket.rocket_name + " will be launched on the " + this.convertDate(nextLaunch.launch_date_local) + " from " + nextLaunch.launch_site.site_name,
        attachments: ['https://gazettereview.com/wp-content/uploads/2015/11/SpaceX-Logo.jpg'],
        data: nextLaunch,
        sticky: true,
        wakeup: true,
        autoClear: false,
        led: { color: '#FF00FF', on: 500, off: 500 },
        actions: actions
      };

      this.localNotifications.schedule(options);

      this.localNotifications.on('click').subscribe(notification => {
        console.log("click", notification);

        if (notification) {
          this.navCtrl.push(LaunchDetailsPage, notification.data).catch(err => console.error(err));
        }
      });

      this.localNotifications.on('next-details').subscribe(notification => {
        console.log("next-details", notification);

        if (notification) {
          this.navCtrl.push(LaunchDetailsPage, notification.data).catch(err => console.error(err));
        }
      });

      this.localNotifications.on('next-ignore').subscribe(notification => {
        console.log("next-ignore", notification);

        if (notification) {
          this.localNotifications.clear(notification.id).catch(err => console.error(err));
        }
      });

      this.localNotifications.on('next-reddit').subscribe(notification => {
        console.log("next-reddit", notification);

        if (notification) {
          window.open(notification.data.links.reddit_campaign,'_system', 'location=yes');
        }
      });
    });
  }
}
