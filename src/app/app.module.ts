import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { HomePageModule } from "../pages/home/home.module";
import {AboutPageModule} from "../pages/about/about.module";
import {CapsulesPageModule} from "../pages/capsules/capsules.module";
import {CapsuleDetailsPageModule} from "../pages/capsule-details/capsule-details.module";
import {DetailedCapsuleDetailsPageModule} from "../pages/detailed-capsule-details/detailed-capsule-details.module";
import {DetailedCapsulesPageModule} from "../pages/detailed-capsules/detailed-capsules.module";
import {DetailedCoreDetailsPageModule} from "../pages/detailed-core-details/detailed-core-details.module";
import {DetailedCoresPageModule} from "../pages/detailed-cores/detailed-cores.module";
import {HistoryPageModule} from "../pages/history/history.module";
import {LaunchpadDetailsPageModule} from "../pages/launchpad-details/launchpad-details.module";
import {LaunchpadsPageModule} from "../pages/launchpads/launchpads.module";
import {RocketDetailsPageModule} from "../pages/rocket-details/rocket-details.module";
import {RocketsPageModule} from "../pages/rockets/rockets.module";
import {LaunchesPageModule} from "../pages/launches/launches.module";
import { HttpClientModule } from "@angular/common/http";
import { SpaceXProvider } from '../providers/space-x/space-x';
import {LaunchDetailsPageModule} from "../pages/launch-details/launch-details.module";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {ChartsModule} from "ng2-charts";
import {DirectivesModule} from "../directives/directives.module";
import {FiltersModalPageModule} from "../pages/launches/filters-modal/filters-modal.module";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    AboutPageModule,
    CapsuleDetailsPageModule,
    CapsulesPageModule,
    DetailedCapsuleDetailsPageModule,
    DetailedCapsulesPageModule,
    DetailedCoreDetailsPageModule,
    DetailedCoresPageModule,
    FiltersModalPageModule,
    HistoryPageModule,
    HomePageModule,
    LaunchesPageModule,
    LaunchpadDetailsPageModule,
    LaunchDetailsPageModule,
    LaunchpadsPageModule,
    RocketDetailsPageModule,
    RocketsPageModule,
    HttpClientModule,
    DirectivesModule,
    IonicModule.forRoot(MyApp),
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    LocalNotifications,
    SpaceXProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
